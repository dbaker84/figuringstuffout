﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float movemultiplier;
	private Rigidbody2D rb;

	// Update is called once per frame
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
	}

	void Update ()
	{
		Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
 		Vector3 dir = Input.mousePosition - pos;
 		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
 		transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}

	void FixedUpdate ()
	{
		rb.AddForce(new Vector3 (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f) * movemultiplier);
	}
}
