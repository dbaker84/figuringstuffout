﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PlayerGenerator : MonoBehaviour {

	public List<string> FirstNames = new List<string>();
	public TextAsset FirstNameList;
	public List<string> LastNames = new List<string>();
	public TextAsset LastNameList;
	public List<string> AnimalNames = new List<string>();
	public TextAsset AnimalNamesList;
	public GameObject PlayerPrefab;

	// Use this for initialization
	void Start () {

        FirstNames.AddRange(FirstNameList.text.Split("\n"[0]));
        LastNames.AddRange(LastNameList.text.Split("\n"[0]));
        AnimalNames.AddRange(AnimalNamesList.text.Split("\n"[0]));

		GameObject team = new GameObject("Team");

		for(int i = 0; i < 10; i++)
		{
			GameObject tempplayer = Instantiate(PlayerPrefab);
			tempplayer.GetComponent<PlayerStats>().FirstName =  FirstNames[Random.Range(0, (FirstNames.Count - 1))];
			tempplayer.GetComponent<PlayerStats>().LastName =  LastNames[Random.Range(0, (LastNames.Count - 1))];
			tempplayer.GetComponent<PlayerStats>().NickName =  AnimalNames[Random.Range(0, (AnimalNames.Count - 1))];			
			tempplayer.GetComponent<PlayerStats>().SetFullName();
			tempplayer.name = tempplayer.GetComponent<PlayerStats>().FullName;
			tempplayer.transform.SetParent(team.transform);

		}
	}



}
