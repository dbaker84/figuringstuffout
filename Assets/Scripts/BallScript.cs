﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour {

	public float PushForce;
	public float TorqueForce;
	public bool Pushed = false;
	public Rigidbody2D rb;
	public GameObject PlayerParent;


	void Start ()
	{
		rb = GetComponent<Rigidbody2D>();
		PlayerParent = transform.parent.gameObject;
	}

	void Update()
	{
		if(transform.parent != null)
		{
			transform.position = transform.parent.position;
			transform.rotation = transform.parent.transform.rotation;
		}
	}
	
	void FixedUpdate () {
		if(!Pushed)
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				transform.parent = null;
				transform.rotation = PlayerParent.transform.rotation;
				Pushed = true;
				rb.AddRelativeForce(Vector3.right * PushForce);
			}
		}
		if(Pushed)
		{
			rb.AddForceAtPosition(new Vector2(rb.velocity.y, -rb.velocity.x) * TorqueForce * Input.GetAxis("Spin"), new Vector2(transform.position.x + 1, transform.position.y));
			if(Input.GetKeyDown(KeyCode.R))
			{
				transform.position = PlayerParent.transform.position;
				transform.parent = PlayerParent.transform;
				Pushed = false;
				rb.velocity = Vector2.zero;
				rb.angularVelocity = 0.0f;

			}
		}
	}
}
