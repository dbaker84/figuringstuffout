﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

	public string FirstName;
	public string LastName;
	public string FullName;
	public string NickName;

	public void SetFullName ()
	{
		if(NickName != "")
			FullName = FirstName + " \"" + NickName + "\" " + LastName;
		else
			FullName = FirstName + " " + LastName;
	}




}
